export const NAVLINKS = [
  {
    routerLink: '/home',
    intitule: 'Accueil'
  },
  {
    routerLink: '/items/list',
    intitule: 'Liste'
  },
  {
    routerLink: '/items/add',
    intitule: 'Ajouter'
  }
];
