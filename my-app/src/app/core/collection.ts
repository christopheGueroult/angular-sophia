import { Item } from '../shared/models/item.model';
import { State } from '../shared/enums/state.enum';

export const COLLECTION: Item[] = [
  {
    id: 'a1',
    name: 'Christophe',
    reference: '1235',
    state: State.ALIVRER
  },
  {
    id: 'b1',
    name: 'Nadine',
    reference: '2546',
    state: State.ENCOURS
  },
  {
    id: 'c1',
    name: 'Marc',
    reference: '5522',
    state: State.LIVREE
  }
];
