import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { ItemComponent } from './components/item/item.component';
import { NavComponent } from './components/nav/nav.component';
import { StateDirective } from './directives/state/state.directive';
import { FormComponent } from './components/form/form.component';

@NgModule({
  imports: [
    CommonModule,
    NgbCollapseModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    NavComponent,
    ItemComponent,
    StateDirective,
    FormComponent
  ],
  exports: [
    NavComponent,
    ItemComponent,
    StateDirective,
    FormComponent
  ]
})
export class SharedModule { }
